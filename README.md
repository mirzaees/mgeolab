# Miami Geodesi Lab (mgeolab)

This container includes following geodesy tools:
[MinSAR](https://github.com/geodesymiami/rsmas_insar), [isce2](https://github.com/isce-framework/isce2), [MintPy](https://github.com/insarlab/MintPy), [pyaps3](https://github.com/AngeliqueBenoit/pyaps3), [MiNoPy](https://github.com/geodesymiami/MiNoPy), [geodmod](https://github.com/geodesymiami/geodmod), [MimtPy](https://github.com/geodesymiami/MimtPy)


If you are not familiar with containers, read the following documents:

https://containers-at-tacc.readthedocs.io/en/latest


## Prerequisites

git clone https://github.com/geodesymiami/accounts.git ~/accounts ;

Setting environment variable:

export SINGULARITY_BINDPATH="/tmp:/home/jovyan"

For sourcing (University of Miami):
alias s.bi='source ~/accounts/platforms_defaults.bash; source ~/accounts/environment.bash; source ~/accounts/alias.bash; source ~/accounts/login_alias.bash;'

## Run the container from the registry:
`docker run -d --rm -p 8888:8888 -v $HOME:/home/jovyan/work -v $WORKDIR:/home/jovyan/insarlab -e JUPYTER_LAB_ENABLE=yes --name mgeolab registry.gitlab.com/mirzaees/mgeolab:0.3 start.sh jupyter lab --LabApp.token=''`

## Running with Singularity

https://sylabs.io/singularity-desktop-macos/

`singularity pull mgeolab.sif docker://registry.gitlab.com/mirzaees/mgeolab:0.3`

`singularity exec mgeolab.sif minsarApp.bash -h`


FROM jupyter/scipy-notebook:3395de4db93a
MAINTAINER Sara Mirzaee "sara.mirzaee@rsmas.miami.edu"

#############################################
### System Libraries and Dependencies
#############################################

USER $NB_UID
COPY conda.txt /home/jovyan
RUN conda install --yes --file /home/jovyan/conda.txt

RUN conda clean --all -f -y
# Activate ipywidgets extension in the environment that runs the notebook server

RUN jupyter nbextension enable --py widgetsnbextension --sys-prefix && \
    jupyter nbextension enable rise --py --sys-prefix && \
    jupyter lab build --dev-build=False && \
    npm cache clean --force && \
    rm -rf $CONDA_DIR/share/jupyter/lab/staging && \
    rm -rf /home/$NB_USER/.cache/yarn && \
    rm -rf /home/$NB_USER/.node-gyp && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER


#############################################
### Jupyter Extensions
#############################################
RUN python3 -m pip install jupyter_nbextensions_configurator jupyter_contrib_nbextensions jupyter_dashboards ipympl 
RUN conda install hide_code
RUN jupyter contrib nbextension install --user
RUN jupyter nbextensions_configurator enable --user
RUN jupyter nbextension enable --py hide_code
RUN jupyter serverextension enable --py hide_code
# Install the jupyter dashboards.
RUN jupyter dashboards quick-setup --sys-prefix
RUN jupyter nbextension enable jupyter_dashboards --py --sys-prefix
# Install interactive matplotlib
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager
RUN jupyter labextension install jupyter-matplotlib jupyter-threejs jupyterlab-datawidgets


#############################################
### Libraries 
#############################################

RUN python3 -m pip install git+https://github.com/insarlab/PySolid.git
RUN python3 -m pip install git+https://github.com/tylere/pykml.git
RUN conda install isce2 -c conda-forge --yes

#############################################
### Libraries and Dependencies
#############################################
## For MintPy
RUN GEOS_DIR=/opt/conda/lib/ CPATH=/opt/conda/include/ python3 -m pip install git+https://github.com/matplotlib/basemap.git
RUN python3 -m pip install git+https://github.com/tylere/pykml.git

USER root
RUN apt-get update && \
    apt-get install -y ssh wget rsync bzip2 csh vim git autoconf gcc g++ gfortran libmotif-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


RUN mkdir /usr/local/development
COPY rsmas_insar.zip /usr/local/development
RUN unzip /usr/local/development/rsmas_insar.zip -d /usr/local/development

##################### build Snaphu and MiNoPy #################
RUN cd /usr/local/development/rsmas_insar/3rdparty/snaphu-v2.0.4/src && make && make install
RUN cd /usr/local/development/rsmas_insar/sources/MiNoPy/minopy/lib && python3 setup.py
COPY accounts /home/jovyan/accounts
RUN cd /usr/local/development/rsmas_insar/setup && ./install_credential_files.csh

RUN cp -p /usr/local/development/rsmas_insar/minsar/additions/isce/logging.conf /opt/conda/lib/python3.*/site-packages/isce/defaults/logging/logging.conf

RUN cp -p /usr/local/development/rsmas_insar/minsar/additions/mintpy/smallbaselineApp_auto.cfg  /usr/local/development/rsmas_insar/sources/MintPy/mintpy/defaults/
RUN cp -p /usr/local/development/rsmas_insar/minsar/additions/mintpy/plot_smallbaselineApp.sh /usr/local/development/rsmas_insar/sources/MintPy/mintpy/sh/
RUN cp -r /usr/local/development/rsmas_insar/sources/isce2/contrib/stack/* /opt/conda/share/isce2

RUN rm -rf /tmp/*
RUN ldconfig
###############################################
USER $NB_UID

RUN conda clean --all -f -y

#ENV MGEOLAB mgeolab_0.1.sif

ENV RSMASINSAR_HOME /usr/local/development/rsmas_insar
ENV CPL_ZIP_ENCODING UTF-8
ENV OPERATIONS /home/jovyan/logs

ENV SSARAHOME ${RSMASINSAR_HOME}/3rdparty/SSARA
ENV ISCE_HOME /opt/conda/lib/python3.8/site-packages/isce
ENV ISCE_STACK /opt/conda/share/isce2
ENV MINTPY_HOME ${RSMASINSAR_HOME}/sources/MintPy
ENV MINOPY_HOME ${RSMASINSAR_HOME}/sources/MiNoPy
ENV MIMTPY_HOME ${RSMASINSAR_HOME}/sources/MimtPy


ENV GEODMOD_HOME ${RSMASINSAR_HOME}/sources/geodmod
ENV SAMPLESDIR ${RSMASINSAR_HOME}/samples
ENV DASK_CONFIG ${MINTPY_HOME}/mintpy/defaults/
ENV LAUNCHER_DIR ${RSMASINSAR_HOME}/3rdparty/launcher
ENV LAUNCHER_PLUGIN_DIR ${LAUNCHER_DIR}/plugins

ENV PYTHON3DIR /opt/conda
ENV CONDA_ENVS_PATH /opt/conda/envs
ENV CONDA_PREFIX ${PYTHON3DIR}
ENV PROJ_LIB ${PYTHON3DIR}/share/proj
ENV GDAL_DATA ${PYTHON3DIR}/share/gdal
ENV PYTHONWARNINGS "ignore"

ENV PYTHONPATH ${PYTHONPATH}:${MINTPY_HOME}:${INT_SCR}:${PYTHON3DIR}/lib/python3.8/site-packages:${ISCE_HOME}:${ISCE_HOME}/components:${MINOPY_HOME}:${MIMTPY_HOME}:${RSMASINSAR_HOME}:${RSMASINSAR_HOME}/sources/rsmas_tools:${RSMASINSAR_HOME}/3rdparty/PyAPS/pyaps3:${RSMASINSAR_HOME}/minsar/utils/ssara_ASF:${RSMASINSAR_HOME}/sources


ENV PATH ${PYTHON3DIR}/bin:${ISCE_HOME}/applications:${ISCE_HOME}/bin:${ISCE_HOME}/applications:${ISCE_HOME}/bin:${ISCE_STACK}:${PATH}:${SSARAHOME}:${MINOPY_HOME}/minopy:${MIMTPY_HOME}/mimtpy:${RSMASINSAR_HOME}/minsar:${RSMASINSAR_HOME}/minsar/utils:${RSMASINSAR_HOME}/minsar:${RSMASINSAR_HOME}/minsar/utils/ssara_ASF:${RSMASINSAR_HOME}/sources/MimtPy:${MINTPY_HOME}/mintpy:${MINTPY_HOME}/sh:${PROJ_LIB}:${RSMASINSAR_HOME}/3rdparty/tippecanoe:${RSMASINSAR_HOME}/sources/insarmaps_scripts:${DASK_CONFIG}:${RSMASINSAR_HOME}/3rdparty/snaphu/bin


ENV LD_LIBRARY_PATH /usr/lib/x86_64-linux-gnu #:${PYTHON3DIR}/lib
ENV OMP_NUM_THREADS 2
